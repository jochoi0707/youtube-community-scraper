from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from time import sleep

URL = "https://www.youtube.com/post/UgxFxRRLBG2CZVTkTtt4AaABCQ"

CSS_CLASS_ITEMS = "ytd-item-section-renderer"
ID_AUTHOR = "author-text"

service = Service("./chromedriver")
options = webdriver.ChromeOptions()
driver = webdriver.Chrome(service=service, options=options)

driver.get(URL)
# for _ in range(6):
#     driver.execute_script('window.scrollTo(0, 1000000);')
#     sleep(2)
sleep(3)
items = driver.find_elements_by_class_name(CSS_CLASS_ITEMS)
for item in items:
    aa = item.find_elements_by_tag_name("a")
    if not aa:
        continue
    aa = [a.text for a in aa if a.text]
    print(aa)

driver.close()
