from selenium import webdriver
from selenium.webdriver.chrome.service import Service
from time import sleep
import csv

service = Service("./chromedriver")
options = webdriver.ChromeOptions()
driver = webdriver.Chrome(service=service, options=options)

with open("data.csv", "r") as f:
    reader = csv.reader(f, delimiter=",")
    count = 0
    for username, n_weeks, channel_url in reader:
        count += 1
        channel_url += "/channels"

        driver.get(channel_url)

        sleep(2)

        channels = driver.find_elements_by_class_name("ytd-grid-channel-renderer")
        if not channels:
            continue

        for _ in range(10):
            driver.execute_script("window.scrollTo(0, 100000);")
            sleep(2)

        if count > 2:
            break

        channels = driver.find_elements_by_class_name("ytd-grid-channel-renderer")
        channels = [channel.text for channel in channels if channel.text]
        print(channels)

# with open("data.csv", mode="w") as csv_file:
#     writer = csv.writer(csv_file, delimiter=",", quotechar='"')
#     for item in items:
#         anchor_tags = item.find_elements_by_tag_name("a")
#         if not anchor_tags:
#             continue
#         aa = [a.text for a in anchor_tags if a.text]
#         hrefs = [a.get_attribute("href") for a in anchor_tags]
#         hrefs = [href for href in hrefs if href and "channel" in href]
#         if not hrefs:
#             continue
#         channel_url = hrefs[0]
#         if len(aa) in (3, 4):
#             username, n_weeks, *_ = aa
#             print(username, n_weeks, channel_url)
#             writer.writerow([username, n_weeks, channel_url])

driver.close()
