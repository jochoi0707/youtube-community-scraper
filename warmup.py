from math import dist


def perimeter(points):
    point1, point2, point3 = points
    return round(dist(point1, point2) + dist(point2, point3) + dist(point1, point3), 2)


ans = perimeter([[15, 7], [5, 22], [11, 1]])
exp = 47.08
assert ans == exp, f"expected {exp}, got {ans}"
ans = perimeter([[0, 0], [0, 1], [1, 0]])
exp = 3.41
assert ans == exp, f"expected {exp}, got {ans}"
ans = perimeter([[-10, -10], [10, 10], [-10, 10]])
exp = 68.28
assert ans == exp, f"expected {exp}, got {ans}"

print("Everything is okay!")
